import { equal, deepStrictEqual } from "assert";
import fetch from "node-fetch";
// import gameOfLife from "../src";

describe("Game of Life", () => {
  it("Load a page on production", async () => {
    const response = await fetch("http://localhost:8080/");
    equal(response.ok, true);

    const content = await response.text();
    equal(content.includes("Game of life"), true);
  });
});
